import React from 'react'
import Login from '../pages/login'


class Authentication extends React.Component {
  state = { authenticated: null }

  successfulLogin = () => {
    this.setState({ authenticated: true })
  }

  logout = () => {
    this.setState({ authenticated: false })
  }

  componentDidMount() {
    if (window.localStorage.getItem('authorization') !== null) {
      this.setState({ authenticated: true })
    } else {
      this.setState({ authenticated: false })
    }
  }

  render() {
    if (this.state.authenticated === null) {
      return <div>Carregando...</div>
    }

    if (this.state.authenticated === true) {
      return <>{this.props.children}</>
    }
    if (this.state.authenticated === false) {
      return <Login login={this.successfulLogin}></Login>
    }

  }
}

export default Authentication