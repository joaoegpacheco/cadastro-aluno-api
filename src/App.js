import Power32 from "@carbon/icons-react/lib/power/32";
import {
  Header,
  HeaderName,
  HeaderGlobalAction,
  HeaderGlobalBar,
  HeaderMenuItem,
  HeaderNavigation,
} from "carbon-components-react/lib/components/UIShell";
import React from "react";
import { withRouter, Route, Switch } from "react-router-dom";
import styled from "styled-components";

import FeedbackForm from "./pages/feedback-form";
import FeedbacksList from "./pages/feedbacks-list";
import Login from "./pages/login";
import UserForm from "./pages/user-form";
import UserList from "./pages/user-list";
import kenzie from './img/kenzie.png'

class App extends React.Component {
  state = {
    authenticated: null,
  };

  successfulLogin = () => {
    this.setState({ authenticated: true });
    this.props.history.push("/list");
  };

  logout = () => {
    this.setState({ authenticated: false });
  };

  componentDidMount() {
    if (window.localStorage.getItem("authorization") !== null) {
      this.setState((state) => {
        return { authenticated: true };
      });
    } else {
      this.setState({ authenticated: false });
    }
  }

  render() {
    if (this.state.authenticated === null) return <div>Carregando...</div>;
    if (this.state.authenticated === false) {
      return (
        <Container>
          <Header aria-label="Secretaria Kenize Academy">
            <HeaderName prefix="Secretaria">Kenzie Academy</HeaderName>
            <HeaderNavigation aria-label="Navigation">
              <HeaderMenuItem href="/form">Cadastro de Aluno</HeaderMenuItem>
              <HeaderMenuItem href="/login">Login</HeaderMenuItem>
            </HeaderNavigation>
            <HeaderGlobalBar />
          </Header>
          <ContentContainer>
            <Switch>
              <Route path="/form">
                <UserForm />
              </Route>
              <Route path="/login">
                <Login login={this.successfulLogin} />
              </Route>
              <Route path="/"> 
                <img src={ kenzie } alt="" width={800} />
              </Route>
            </Switch>
          </ContentContainer>
        </Container>
      );
    }
    if (this.state.authenticated === true) {
      return (
        <Container>
          <Header aria-label="Secretaria Kenize Academy">
            <HeaderName prefix="Secretaria">Kenzie Academy</HeaderName>
            <HeaderNavigation aria-label="Navigation">
              <HeaderMenuItem href="/list">Lista de Alunos</HeaderMenuItem>
            </HeaderNavigation>
            <HeaderGlobalBar>
              <HeaderGlobalAction
                aria-label="Logout"
                onClick={() => {
                  window.localStorage.clear();
                  this.setState(
                    { authenticated: false },
                    this.props.history.push("/login")
                  );
                }}
              >
                <Power32 />
              </HeaderGlobalAction>
            </HeaderGlobalBar>
          </Header>
          <ContentContainer>
            <Switch>
              <Route exact path="/list">
                <UserList />
              </Route>
              <Route exact path="/list/:userId/feedbacks">
                <FeedbacksList />
              </Route>
              <Route exact path="/list/:userId/create">
                <FeedbackForm />
              </Route>
            </Switch>
          </ContentContainer>
        </Container>
      );
    }
  }
}

const ContentContainer = styled.div`
  padding-top: 48px;
  @media (min-width: 1201px) {
    padding-left: 256px;
    padding-right: 256px;
  }
  @media (max-width: 1201px) {
    padding-left: 48px;
    padding-right: 48px;
  }
`;
const Container = styled.div`
  padding-top: 48px;
  min-height: 100vh;
`;

export default withRouter(App);
