import {
  StructuredListWrapper,
  StructuredListHead,
  StructuredListBody,
  StructuredListRow,
  StructuredListCell,
} from "carbon-components-react";
import React from "react";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";

class UserList extends React.Component {
  state = { users: [] };

  //criar request para obter usuários
  fetchUsers = () => {
    fetch("https://ka-users-api.herokuapp.com/users", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("authorization")
      }
    })
      .then(res => res.json())
      .then(res => this.setState({ users: res }))
  }

  componentDidMount = () => {
    this.fetchUsers()
  }

  render() {
    return (
      <StructuredListWrapper>
        <StructuredListHead>
          <StructuredListRow head>
            <StructuredListCell head>Name</StructuredListCell>
            <StructuredListCell head>User</StructuredListCell>
            <StructuredListCell head>Email</StructuredListCell>
            <StructuredListCell head>Feedbacks</StructuredListCell>
          </StructuredListRow>
        </StructuredListHead>
        <StructuredListBody>
          {this.state.users.map((user, key) => {
            return (
              <StructuredListRow key={key}>
                <StructuredListCell>{user.name}</StructuredListCell>
                <StructuredListCell>{user.user}</StructuredListCell>
                <StructuredListCell>{user.email}</StructuredListCell>
                <StructuredListCell><Link to={`/list/${user.id}/feedbacks`}>{user.name}</Link></StructuredListCell>
              </StructuredListRow>
          )
        })}
        </StructuredListBody>
      </StructuredListWrapper>
    );
  }
}

export default withRouter(UserList);