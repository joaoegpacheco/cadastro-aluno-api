import { TextInput, FormGroup, Button } from "carbon-components-react";
import React from "react";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

class UserForm extends React.Component {
  state = {
    input: {
      name: "",
      cpf: "",
      cellphone: "",
      address: "",
      user: "",
      email: "",
      password: "",
      passwordConfirmation: "",
    },

    errors: {
      name: [],
      cpf: [],
      cellphone: [],
      address: [],
      user: [],
      email: [],
      password: [],
      passwordConfirmation: [],
    },
  };

  runValidations = () => {
    const nameErrors = this.validateField("name", [
      "required",
      {
        pattern: /(^[A-ZÀ-Ÿ][A-zÀ-ÿ']+\s([A-zÀ-ÿ']\s?)*[A-ZÀ-Ÿ][A-zÀ-ÿ']+$)/g,
        errorMessage: "Nome completo inválido",
      },
    ]);
    const cpfErrors = this.validateField("cpf", [
      "required",
      {
        pattern: /(^\d{3}\.\d{3}\.\d{3}-\d{2}$)|(^\d{11}$)/g,
        errorMessage: "CPF inválido",
      },
    ]);
    const cellphoneErrors = this.validateField("cellphone", [
      "required",
      {
        pattern: /(\(?\d{2}\)?\s)?(\d{4,5}-\d{4})/g,
        errorMessage: "Número de telefone inválido",
      },
    ]);
    const addressErrors = this.validateField("address", ["required"]);
    const emailErrors = this.validateField("email", [
      "required",
      { pattern: /\S+@\S+\.\S+/, errorMessage: "Email inválido" },
    ]);

    const userErrors = this.validateField("user", ["required"]);
    const passwordErrors = this.validateField("password", ["required"]);
    const passwordConfirmationErrors = this.validateField(
      "passwordConfirmation",
      ["required", "passwordMatch"]
    );

    this.setState((state) => {
      return {
        ...state,
        errors: {
          name: nameErrors,
          cpf: cpfErrors,
          cellphone: cellphoneErrors,
          address: addressErrors,
          email: emailErrors,
          user: userErrors,
          password: passwordErrors,
          passwordConfirmation: passwordConfirmationErrors,
        },
      };
    });

    if (
      nameErrors.length === 0 &&
      cpfErrors.length === 0 &&
      cellphoneErrors.length === 0 &&
      addressErrors.length === 0 &&
      emailErrors.length === 0 &&
      userErrors.length === 0 &&
      passwordErrors.length === 0 &&
      passwordConfirmationErrors.length === 0
    ) {
      this.createUser();
    }
  };

  checkStatus = (response) => {
    if (response.status >= 200 && response.status < 300) {
      return response;
    }
    throw response;
  };

  parseJSON(response) {
    return response.json();
  }

  createUser = () => {
    //TODO: Fazer request para criar usuário
    const {name, cpf, cellphone, address, user, email, password, password_confirmation} = this.state.input
    fetch("https://ka-users-api.herokuapp.com/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        "user": {
          "name": name,
          "cpf": cpf,
          "cellphone": cellphone,
          "address": address,
          "user": user,
          "email": email,
          "password": password,
          "password_confirmation": password_confirmation
        }
      })
    }) 
      .then(res => res.json())
      .then(res => {
        this.props.history.replace("/login")
        console.log(res)
      })
  };

  validateField = (name, rules) => {
    const input = this.state.input[name].trim();

    const errors = [];
    rules.forEach((rule) => {
      if (rule === "required") {
        if (input === "" || input === undefined || input === null) {
          errors.push("Não pode ficar em branco");
          return;
        }
        return;
      }

      if (rule.pattern) {
        const regex = new RegExp(rule.pattern);
        if (!regex.test(input)) {
          errors.push(rule.errorMessage);
        }
      }

      if (rule === "passwordMatch") {
        if (
          this.state.input.password !== this.state.input.passwordConfirmation
        ) {
          errors.push("As passwords não são iguais");
        }
      }
    });

    return [...errors];
  };

  handleChange = (field, input) => {
    this.setState((state) => {
      return { ...state, input: { ...state.input, [field]: input } };
    });
  };

  render() {
    return (
      <>
        <ContentHeader>Formulário de admissão:</ContentHeader>
        <FormGroup legendText="">
          <TextInput
            labelText="Nome Completo"
            id="name"
            invalidText={this.state.errors.name[0]}
            invalid={this.state.errors.name.length > 0 && true}
            onChange={(e) => this.handleChange("name", e.target.value)}
            value={this.state.input.name}
          />
          <TextInput
            labelText="CPF"
            id="cpf"
            invalidText={this.state.errors.cpf[0]}
            invalid={this.state.errors.cpf.length > 0 && true}
            onChange={(e) => this.handleChange("cpf", e.target.value)}
            value={this.state.input.cpf}
          />
          <TextInput
            labelText="Celular formato: (dd) 99999-9999"
            id="cellphone"
            invalidText={this.state.errors.cellphone[0]}
            invalid={this.state.errors.cellphone.length > 0 && true}
            onChange={(e) => this.handleChange("cellphone", e.target.value)}
            value={this.state.input.cellphone}
          />
          <TextInput
            labelText="Endereço"
            id="address"
            invalidText={this.state.errors.address[0]}
            invalid={this.state.errors.address.length > 0 && true}
            onChange={(e) => this.handleChange("address", e.target.value)}
            value={this.state.input.address}
          />
          <TextInput
            labelText="User"
            id="user"
            invalidText={this.state.errors.user[0]}
            invalid={this.state.errors.user.length > 0 && true}
            onChange={(e) => this.handleChange("user", e.target.value)}
            value={this.state.input.user}
          />
          <TextInput
            labelText="Email"
            id="email"
            invalidText={this.state.errors.email[0]}
            invalid={this.state.errors.email.length > 0 && true}
            onChange={(e) => this.handleChange("email", e.target.value)}
            value={this.state.input.email}
          />
          <TextInput
            type="password"
            labelText="Password"
            id="password"
            invalidText={this.state.errors.password[0]}
            invalid={this.state.errors.password.length > 0 && true}
            onChange={(e) => this.handleChange("password", e.target.value)}
            value={this.state.input.password}
          />
          <TextInput
            type="password"
            labelText="Confirmação da Password"
            id="passwordConfirmation"
            invalidText={this.state.errors.passwordConfirmation[0]}
            invalid={this.state.errors.passwordConfirmation.length > 0 && true}
            onChange={(e) =>
              this.handleChange("passwordConfirmation", e.target.value)
            }
            value={this.state.input.passwordConfirmation}
          />
        </FormGroup>
        <Button
          type="submit"
          onClick={() => {
            this.runValidations();
          }}
        >
          Confirmar
        </Button>
      </>
    );
  }
}

export default withRouter(UserForm);

const ContentHeader = styled.h3`
  padding-bottom: 24px;
`;
