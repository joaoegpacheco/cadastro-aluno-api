import { TextInput, FormGroup, Button } from "carbon-components-react";
import React from "react";
import { withRouter } from "react-router-dom";
class Login extends React.Component {
  state = {
    input: {
      user: "",
      password: "",
    },
    errors: {
      user: [],
      password: [],
    },
  };

  handleChange = (field, input) => {
    this.setState((state) => {
      return { ...state, input: { ...state.input, [field]: input } };
    });
  };

  handleClick = () => {
    //TODO: Criar request para fazer login
    const {user, password} = this.state.input
    fetch("https://ka-users-api.herokuapp.com/authenticate", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        "user": user,
        "password": password
      })
    })
      .then(res => res.json())
      .then(res => {
        if (res.auth_token) {
          localStorage.setItem("authorization", res.auth_token)
          this.props.login()
        }

      })
      .catch(error => console.log(error))

  };

  render() {
    return (
      <>
        <FormGroup legendText="">
          <TextInput
            labelText="user"
            id="user"
            invalidText={this.state.errors.user[0]}
            invalid={this.state.errors.user.length > 0 && true}
            onChange={(e) => this.handleChange("user", e.target.value)}
            value={this.state.input.user}
          />
          <TextInput
            labelText="password"
            type="password"
            id="password"
            invalidText={this.state.errors.password[0]}
            invalid={this.state.errors.password.length > 0 && true}
            onChange={(e) => this.handleChange("password", e.target.value)}
            value={this.state.input.password}
          />
        </FormGroup>
        <Button onClick={() => this.handleClick()}>Login</Button>
      </>
    );
  }
}

export default withRouter(Login);
