import { TextInput, FormGroup, Button } from "carbon-components-react";
import React from "react";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

class FeedbackForm extends React.Component {
  state = {
    input: {
      name: "",
      comment: "",
      grade: "",
    },
    errors: {
      name: [],
      comment: [],
      grade: [],
    },
    loading: "inactive",
  };

  runValidations = () => {
    const nameErrors = this.validateField("name", ["required"]);
    const commentErrors = this.validateField("name", ["required"]);
    const gradeErrors = this.validateField("name", ["required"]);

    this.setState((state) => {
      return {
        ...state,
        errors: {
          name: nameErrors,
          comment: commentErrors,
          grade: gradeErrors,
        },
      };
    });

    if (
      nameErrors.length === 0 &&
      commentErrors.length === 0 &&
      gradeErrors.length === 0
    ) {
      this.createFeedback();
    } else {
    }
  };

  createFeedback = () => {
    //criar request para criar feedback
    const {name, comment, grade} = this.state.input

    fetch(`https://ka-users-api.herokuapp.com/users/${this.props.match.params.userId}/feedbacks`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("authorization")
      },
      body: JSON.stringify({
        "feedback": {
          "name": name,
          "comment": comment,
          "grade": grade
        }
      })
    }) 
      .then(res => res.json())
      .then(res => this.setState({ input: res }))
      .then(this.props.history.replace(`/list/${this.props.match.params.userId}/feedbacks`))  
  };

  validateField = (name, rules) => {
    const input = this.state.input[name].trim();

    const errors = [];
    rules.forEach((rule) => {
      if (rule === "required") {
        if (input === "" || input === undefined || input === null) {
          errors.push("Não pode ficar em branco");
          return;
        }
        return;
      }

      if (rule.pattern) {
        const regex = new RegExp(rule.pattern);
        if (!regex.test(input)) {
          errors.push(rule.errorMessage);
        }
      }
    });

    return [...errors];
  };

  handleChange = (field, input) => {
    this.setState((state) => {
      return { ...state, input: { ...state.input, [field]: input } };
    });
  };

  render() {
    return (
      <>
        <ContentHeader>Novo Feedback</ContentHeader>
        <FormGroup legendText="">
          <TextInput
            labelText="Feedback Name"
            id="name"
            invalidText={this.state.errors.name[0]}
            invalid={this.state.errors.name.length > 0 && true}
            onChange={(e) => this.handleChange("name", e.target.value)}
            value={this.state.input.name}
          />
          <TextInput
            labelText="Comment"
            id="comment"
            invalidText={this.state.errors.comment[0]}
            invalid={this.state.errors.comment.length > 0 && true}
            onChange={(e) => this.handleChange("comment", e.target.value)}
            value={this.state.input.comment}
          />
          <TextInput
            labelText="Grade"
            id="grade"
            invalidText={this.state.errors.grade[0]}
            invalid={this.state.errors.grade.length > 0 && true}
            onChange={(e) => this.handleChange("grade", e.target.value)}
            value={this.state.input.grade}
          />
        </FormGroup>
        <Button
          type="submit"
          onClick={() => {
            this.setState((state) => {
              return { ...state, loading: "active" };
            }, this.runValidations);
          }}
        >
          Criar
        </Button>
      </>
    );
  }
}

const ContentHeader = styled.h3`
  padding-bottom: 24px;
`;

export default withRouter(FeedbackForm);
