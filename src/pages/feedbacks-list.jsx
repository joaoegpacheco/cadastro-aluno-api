import {
  StructuredListWrapper,
  StructuredListHead,
  StructuredListBody,
  StructuredListRow,
  StructuredListCell,
  Button,
} from "carbon-components-react";
import React from "react";
import { withRouter } from "react-router-dom";

class FeedbacksList extends React.Component {
  state = { feedbacks: [] };

  //criar request para receber todos os feedbacks de um determinado usuáio
  fetchFeedback = () => {
    fetch(`https://ka-users-api.herokuapp.com/users/${this.props.match.params.userId}/feedbacks`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("authorization")
      }
    })
      .then(res => res.json())
      .then(res => this.setState({ feedbacks: res }))
  }

  componentDidMount = () => {
    this.fetchFeedback()

    //Como estamos utilizando o withRouter, temos acessos a
    //this.props.match.params
    //dessa forma eu consigo receber um parâmetro que está declarado
    //na URL da rota
    //Deem uma olhada nas rotas do App.js :)

    console.log(this.props.match.params.userId);
  }

  render() {
    return (
      <StructuredListWrapper>
        <StructuredListHead>
          <StructuredListRow head>
            <StructuredListCell head>Feedback Name</StructuredListCell>
            <StructuredListCell head>Comment</StructuredListCell>
            <StructuredListCell head>Grade</StructuredListCell>
            <StructuredListCell head>Creator</StructuredListCell>
          </StructuredListRow>
        </StructuredListHead>
        <StructuredListBody>
          {this.state.feedbacks.map((feedback, key) => {
            return (
              <StructuredListRow key={key}>
                <StructuredListCell>{feedback.name}</StructuredListCell>
                <StructuredListCell>{feedback.comment}</StructuredListCell>
                <StructuredListCell>{feedback.grade}</StructuredListCell>
                <StructuredListCell>{feedback.creator.name}</StructuredListCell>
              </StructuredListRow>
          )
        })}
        </StructuredListBody>
        <Button href={"/list/" + this.props.match.params.userId + "/create"}>
          Add Feedback
        </Button>
        <Button href={"/list/"} style={{marginLeft: "20px" }}>
          Voltar
        </Button>
      </StructuredListWrapper>
    );
  }
}

export default withRouter(FeedbacksList);
